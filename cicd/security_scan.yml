include:
  - local: cicd/security_scan_config.yml


.security_scan_base:
  extends:
    - .security_scan_config
  image:
    name: python:3.12.4-alpine3.19
  before_script:
    - apk add --no-cache aws-cli jq envsubst
    - pip install --progress-bar=off requests tabulate
    - aws configure set cli_timestamp_format iso8601
    - aws configure get cli_timestamp_format
    - export APP_VERSION=$(scripts/get_app_version "$RESOURCE_USER_AWS_ASSUME_ROLE")
    - echo "APP_VERSION=$APP_VERSION" >> $GITLAB_ENV
    - |
      export $(printf "AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_SESSION_TOKEN=%s" \
        $(aws sts assume-role \
          --role-arn "${RESOURCE_OWNER_AWS_ASSUME_ROLE}" \
          --role-session-name "gitlab-ci-${CI_PROJECT_NAME}-${CI_PIPELINE_IID}" \
          --query "Credentials.[AccessKeyId,SecretAccessKey,SessionToken]" \
          --output text
        )
      )
  variables:
    sorting: field=FIRST_OBSERVED_AT,sortOrder=ASC
    upper_bound_high: 10.0
    lower_bound_high: 7.0
    upper_bound_medium: 6.9999
    lower_bound_medium: 4.0
    filters_ecr_report: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_medium}, "upperInclusive": ${upper_bound_high}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "EQUALS"}],
        "ecrImageTags": [{"value": "$$APP_VERSION", "comparison": "EQUALS"}]
      }
    filters_ecr_high: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_high}, "upperInclusive": ${upper_bound_high}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "EQUALS"}],
        "ecrImageTags": [{"value": "$$APP_VERSION", "comparison": "EQUALS"}]
      }
    filters_ecr_medium: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_medium}, "upperInclusive": ${upper_bound_medium}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "EQUALS"}],
        "ecrImageTags": [{"value": "$$APP_VERSION", "comparison": "EQUALS"}]
      }
    filters_ecr_untriaged: |
      {
        "severity": [{"value": "UNTRIAGED", "comparison": "EQUALS"}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "EQUALS"}],
        "ecrImageTags": [{"value": "$$APP_VERSION", "comparison": "EQUALS"}]
      }
    filters_resources_report: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_medium}, "upperInclusive": ${upper_bound_high}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "NOT_EQUALS"}]
      }
    filters_resources_high: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_high}, "upperInclusive": ${upper_bound_high}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "NOT_EQUALS"}]
      }
    filters_resources_medium: |
      {
        "inspectorScore": [{"lowerInclusive": ${lower_bound_medium}, "upperInclusive": ${upper_bound_medium}}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "NOT_EQUALS"}]
      }
    filters_resources_untriaged: |
      {
        "severity": [{"value": "UNTRIAGED", "comparison": "EQUALS"}],
        "findingStatus": [{"value": "ARCHIVED", "comparison": "NOT_EQUALS"}, {"value": "CLOSED", "comparison": "NOT_EQUALS"}],
        "resourceType": [{"value": "AWS_ECR_CONTAINER_IMAGE", "comparison": "NOT_EQUALS"}]
      }
    filters_security_hub_report: |
      {
        "SeverityNormalized": [{"Gte": 40, "Lte": 100}],
        "WorkflowStatus": [{"Value": "SUPPRESSED", "Comparison": "NOT_EQUALS"}],
        "RecordState": [{"Value": "ARCHIVED", "Comparison": "NOT_EQUALS"}, {"Value": "CLOSED", "Comparison": "NOT_EQUALS"}],
        "ProductName": [
          {"Value": "Inspector", "Comparison": "NOT_EQUALS"},
          {"Value": "GuardDuty", "Comparison": "NOT_EQUALS"},
          {"Value": "Health", "Comparison": "NOT_EQUALS"}
        ]
      }
    filters_security_hub_high: |
      {
        "SeverityNormalized": [{"Gte": 70, "Lte": 100}],
        "WorkflowStatus": [{"Value": "SUPPRESSED", "Comparison": "NOT_EQUALS"}],
        "RecordState": [{"Value": "ARCHIVED", "Comparison": "NOT_EQUALS"}, {"Value": "CLOSED", "Comparison": "NOT_EQUALS"}],
        "ProductName": [
          {"Value": "Inspector", "Comparison": "NOT_EQUALS"},
          {"Value": "GuardDuty", "Comparison": "NOT_EQUALS"},
          {"Value": "Health", "Comparison": "NOT_EQUALS"}
        ]
      }
    filters_security_hub_medium: |
      {
        "SeverityNormalized": [{"Gte": 40, "Lte": 69}],
        "WorkflowStatus": [{"Value": "SUPPRESSED", "Comparison": "NOT_EQUALS"}],
        "RecordState": [{"Value": "ARCHIVED", "Comparison": "NOT_EQUALS"}, {"Value": "CLOSED", "Comparison": "NOT_EQUALS"}],
        "ProductName": [
          {"Value": "Inspector", "Comparison": "NOT_EQUALS"},
          {"Value": "GuardDuty", "Comparison": "NOT_EQUALS"},
          {"Value": "Health", "Comparison": "NOT_EQUALS"}
        ]
      }
    query_ecr: |
      {
        date: `$CI_JOB_STARTED_AT`,
        ecr_account: {
          id: `$RESOURCE_OWNER_AWS_ACCOUNT_ID`,
          name: `$RESOURCE_OWNER_AWS_ACCOUNT_NAME`
        },
        image_tag: `$$APP_VERSION`,
        findings: findings[].{
          id: @.packageVulnerabilityDetails.vulnerabilityId,
          severity: @.severity,
          score: @.inspectorScore,
          packages: @.packageVulnerabilityDetails.vulnerablePackages[].{
            name: @.name,
            version: @.version
          },
          description: @.description,
          first_observed_at: @.firstObservedAt,
          resources: @.resources[].{
            account: {
              id: `$RESOURCE_USER_AWS_ACCOUNT_ID`,
              name: `$RESOURCE_USER_AWS_ACCOUNT_NAME`
            },
            id: @.id,
            type: @.type,
            tags: @.details.awsEcrContainerImage.imageTags | sort(@)
          }
        }
      }
    query_resources: |
      {
        date: `$CI_JOB_STARTED_AT`,
        account: {
          id: `$RESOURCE_OWNER_AWS_ACCOUNT_ID`,
          name: `$RESOURCE_OWNER_AWS_ACCOUNT_NAME`
        },
        findings: findings[].{
          id: @.packageVulnerabilityDetails.vulnerabilityId,
          severity: @.severity,
          score: @.inspectorScore,
          packages: @.packageVulnerabilityDetails.vulnerablePackages[].{
            name: @.name,
            version: @.version
          },
          description: @.description,
          first_observed_at: @.firstObservedAt,
          resources: @.resources[].{
            account: {
              id: `$RESOURCE_USER_AWS_ACCOUNT_ID`,
              name: `$RESOURCE_USER_AWS_ACCOUNT_NAME`
            },
            id: @.id,
            type: @.type
          }
        }
      }
    query_security_hub: |
      {
        date: `$CI_JOB_STARTED_AT`,
        account: {
          id: `$RESOURCE_OWNER_AWS_ACCOUNT_ID`,
          name: `$RESOURCE_OWNER_AWS_ACCOUNT_NAME`
        },
        findings: Findings[].{
          id: join(`: `, [@.ProductName, @.Title]),
          security_hub_id: @.Id,
          title: @.Title,
          severity: @.Severity.Label,
          score: @.Severity.Normalized,
          compliance: @.Compliance.Status,
          scanner: @.ProductName,
          description: @.Description,
          first_observed_at: @.FirstObservedAt,
          resources: @.Resources[].{
            account: {
              id: `$RESOURCE_USER_AWS_ACCOUNT_ID`,
              name: `$RESOURCE_USER_AWS_ACCOUNT_NAME`
            },
            id: @.Id,
            type: @.Type
          }
        }
      }

.scheduled_scan_base:
  extends: .security_scan_base
  needs: []
  script:
    - aws inspector2 list-findings --sort-criteria="$sorting" --filter-criteria="$(echo "$filters_high" | envsubst)" --query="$(echo "$query" | envsubst)" | tee "$(echo "${artifact_prefix}-high-${artifact_suffix}-${CI_JOB_STARTED_AT}.json" | envsubst)"
    - aws inspector2 list-findings --sort-criteria="$sorting" --filter-criteria="$(echo "$filters_medium" | envsubst)" --query="$(echo "$query" | envsubst)" | tee "$(echo "${artifact_prefix}-medium-${artifact_suffix}-${CI_JOB_STARTED_AT}.json" | envsubst)"
    - aws inspector2 list-findings --sort-criteria="$sorting" --filter-criteria="$(echo "$filters_untriaged" | envsubst)" --query="$(echo "$query" | envsubst)" | tee "$(echo "${artifact_prefix}-untriaged-${artifact_suffix}-${CI_JOB_STARTED_AT}.json" | envsubst)"
    - test -z "$(jq -r .findings[].id ${artifact_prefix}-high-*.json)" || exit 30
    - test -z "$(jq -r .findings[].id ${artifact_prefix}-medium-*.json)" || exit 20
  allow_failure:
    exit_codes:
      - 10
      - 20
  artifacts:
    paths:
      - ${artifact_prefix}-*.json
    when: always
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TYPE == "security-scan"

.resources_scan:
  extends: .scheduled_scan_base
  stage: inspector_resources
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $RESOURCE_USER_AWS_ACCOUNT_ID
    RESOURCE_OWNER_AWS_ACCOUNT_NAME: $RESOURCE_USER_AWS_ACCOUNT_NAME
    filters_high: $filters_resources_high
    filters_medium: $filters_resources_medium
    filters_untriaged: $filters_resources_untriaged
    query: $query_resources
    artifact_prefix: resources-scan
    artifact_suffix: $RESOURCE_OWNER_AWS_ACCOUNT_NAME

.container_scan:
  extends: .scheduled_scan_base
  stage: inspector_containers
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $ECR_ACCOUNT_ID
    RESOURCE_OWNER_AWS_ACCOUNT_NAME: $ECR_ACCOUNT_NAME
    filters_high: $filters_ecr_high
    filters_medium: $filters_ecr_medium
    filters_untriaged: $filters_ecr_untriaged
    query: $query_ecr
    artifact_prefix: container-scan
    artifact_suffix: "$${RESOURCE_USER_AWS_ACCOUNT_NAME}-$${APP_VERSION}"

.security_hub_scan:
  extends: .scheduled_scan_base
  stage: security_hub
  script:
    - |
      set -x
      aws securityhub get-findings --sort-criteria="$sorting" --filters="$filters_high" --query="$query" \
      | jq 'if .findings then .findings[].score /= 10.0 else . end' \
      | tee ${artifact_prefix}-high-${artifact_suffix}-${CI_JOB_STARTED_AT}.json
    - |
      set -x
      aws securityhub get-findings --sort-criteria="$sorting" --filters="$filters_medium" --query="$query" \
      | jq 'if .findings then .findings[].score /= 10.0 else . end' \
      | tee ${artifact_prefix}-medium-${artifact_suffix}-${CI_JOB_STARTED_AT}.json
    - test -z "$(jq -r .findings[].id ${artifact_prefix}-high-${artifact_suffix}-*.json)" || exit 30
    - test -z "$(jq -r .findings[].id ${artifact_prefix}-medium-${artifact_suffix}-*.json)" || exit 20
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $RESOURCE_USER_AWS_ACCOUNT_ID
    RESOURCE_OWNER_AWS_ACCOUNT_NAME: $RESOURCE_USER_AWS_ACCOUNT_NAME
    sorting: Field=FirstObservedAt,SortOrder=asc
    filters_high: $filters_security_hub_high
    filters_medium: $filters_security_hub_medium
    query: $query_security_hub
    artifact_prefix: security_hub
    artifact_suffix: $RESOURCE_USER_AWS_ACCOUNT_NAME

resources_scan_dev:
  extends: .resources_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_DEV
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_dev"

resources_scan_test:
  extends: .resources_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_TEST
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_test"

resources_scan_e2e:
  extends: .resources_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_E2E
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_e2e"

resources_scan_uat:
  extends: .resources_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_UAT
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_uat"

resources_scan_prod:
  extends: .resources_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_PROD
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_prod"

container_scan_dev:
  extends: .container_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_DEV
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_dev"

container_scan_test:
  extends: .container_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_TEST
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_test"

container_scan_e2e:
  extends: .container_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_E2E
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_e2e"

container_scan_uat:
  extends: .container_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_UAT
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_uat"

container_scan_prod:
  extends: .container_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_PROD
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_prod"

security_hub_scan_dev:
  extends: .security_hub_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_DEV
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_dev"

security_hub_scan_test:
  extends: .security_hub_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_TEST
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_test"

security_hub_scan_e2e:
  extends: .security_hub_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_E2E
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_e2e"

security_hub_scan_uat:
  extends: .security_hub_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_UAT
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_uat"

security_hub_scan_prod:
  extends: .security_hub_scan
  variables:
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID_PROD
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_prod"

.security_scan_reporting:
  extends: .security_scan_base
  stage: security_scan_reporting
  dependencies:
    - container_scan_dev
    - container_scan_test
    - container_scan_e2e
    - container_scan_uat
    - container_scan_prod
    - resources_scan_dev
    - resources_scan_test
    - resources_scan_e2e
    - resources_scan_uat
    - resources_scan_prod
    - security_hub_scan_dev
    - security_hub_scan_test
    - security_hub_scan_e2e
    - security_hub_scan_uat
    - security_hub_scan_prod
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $ECR_ACCOUNT_ID
    RESOURCE_USER_AWS_ASSUME_ROLE: ""
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TYPE == "security-scan"
      when: always

upload_to_jira:
  extends: .security_scan_reporting
  script:
    - scripts/scan_reports create --label aws-inspector resources-scan-high-*.json resources-scan-medium-*.json
    - scripts/scan_reports create --label aws-inspector --label ecr container-scan-high-*.json container-scan-medium-*.json
    - scripts/scan_reports create security_hub-high-*.json security_hub-medium-*.json

stats:
  extends: .security_scan_reporting
  script:
    - scripts/scan_reports stats *-high-*.json *-medium-*.json | tee stats-all-findings-${CI_JOB_STARTED_AT}.txt
    - scripts/scan_reports stats resources-scan-high-*.json container-scan-high-*.json resources-scan-medium-*.json container-scan-medium-*.json | tee stats-inspector-vulnerabilities-${CI_JOB_STARTED_AT}.txt
    - scripts/scan_reports stats security_hub-high-*.json security_hub-medium-*.json | tee stats-security_hub-findings-${CI_JOB_STARTED_AT}.txt
  artifacts:
    paths:
      - stats-*.txt

# Deployment time scans

attach_container_scan:
  extends: .attach_security_scan
  script:
    - |
      for repo in $(echo "$ECR_REPOSITORIES" | tr ',' ' '); do
        while [ "$(aws ecr describe-image-scan-findings --repository-name "$repo" --image-id imageTag="$APP_VERSION" --query '@.imageScanStatus.status' --output text)" != "ACTIVE" ]; do
          sleep 10
        done
      done
    - |
      set -x
      aws inspector2 list-findings \
        --sort-criteria="$sorting" \
        --filter-criteria="$(echo "$filters_ecr_report" | envsubst)" \
        --query="$(echo "$query_ecr" | envsubst)" \
      | tee "container-scan-${RESOURCE_USER_AWS_ACCOUNT_NAME}-${APP_VERSION}-${CI_JOB_STARTED_AT}.json"
    - |
      if [ -n "$JIRA_TICKET" ]; then
        set -x
        scripts/scan_reports attach "$JIRA_TICKET" container-scan-"${RESOURCE_USER_AWS_ACCOUNT_NAME}"-*.json
      fi
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $ECR_ACCOUNT_ID
    RESOURCE_OWNER_AWS_ACCOUNT_NAME: $ECR_ACCOUNT_NAME
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_${ENV}"
  artifacts:
    paths:
      - container-scan-*.json
    when: always

attach_resource_scan:
  extends: .attach_security_scan
  script:
    - |
      set -x
      aws inspector2 list-findings \
        --sort-criteria="$sorting" \
        --filter-criteria="$filters_resources_report" \
        --query="$query_resources" \
      | tee "resources-scan-${RESOURCE_USER_AWS_ACCOUNT_NAME}-${CI_JOB_STARTED_AT}.json"
    - |
      if [ -n "$JIRA_TICKET" ]; then
        set -x
        scripts/scan_reports attach "$JIRA_TICKET" resources-scan-*.json
      fi
  variables:
    RESOURCE_OWNER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID
    RESOURCE_OWNER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_${ENV}"
    RESOURCE_USER_AWS_ACCOUNT_ID: $AWS_ACCOUNT_ID
    RESOURCE_USER_AWS_ACCOUNT_NAME: "${AWS_ACCOUNT_NAME_PREFIX}_${ENV}"
  artifacts:
    paths:
      - resources-scan-*.json
    when: always
