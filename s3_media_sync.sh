#!/bin/bash

command="$1"
cache_dir="$HOME/.s3_media_sync"
mkdir -p "$cache_dir"


case "$command" in
  ls)
    (
      cd "$cache_dir" && find . -mindepth 2 -maxdepth 2 -type d | sed s@./@@
    )
    ;;
  down)
    media_bucket=$(aws s3api list-buckets --query "Buckets[*].{name: Name, p: ends_with(Name, 'panelapp-media')}|@[?p].name" --output text)
    local_bucket="$cache_dir/$(date -Idate)/$media_bucket"
    mkdir -p "$local_bucket"
    aws s3 sync --delete "s3://$media_bucket" "$local_bucket"
    ;;
  up)
    media_bucket=$(aws s3api list-buckets --query "Buckets[*].{name: Name, p: ends_with(Name, 'panelapp-media')}|@[?p].name" --output text)
    select bucket in $(cd "$cache_dir" && find . -mindepth 2 -maxdepth 2 -type d | sed s@./@@) abort; do
      if [ "$bucket" = abort ]; then
        exit 3
      fi
      local_bucket="$cache_dir/$bucket"
      if [ -e "$local_bucket" ]; then
        aws s3 sync --delete --sse AES256 "$local_bucket" "s3://$media_bucket"
        break
      fi
    done
    ;;
  rm)
    select bucket in $(cd "$cache_dir" && find . -mindepth 2 -maxdepth 2 -type d | sed s@./@@) all abort; do
      if [ "$bucket" = abort ]; then
        exit 3
      fi
      if [ "$bucket" = all ]; then
        rm -rf "$cache_dir"
        break
      fi
      local_bucket="$cache_dir/$bucket"
      if [ -e "$local_bucket" ]; then
        rm -rf "$local_bucket"
        break
      fi
    done
    ;;

  *)
    # help
    {
      echo "Usage: $0 (ls|down|up|rm)"
      echo
      echo "ls:   list locally cached buckets"
      echo "down: download media bucket into cache"
      echo "up:   upload media bucket from cache"
      echo "rm:   deleted locally cached buckets"
      echo
      echo up and rm are interactive
    } >&2
    exit 2
    ;;
esac
