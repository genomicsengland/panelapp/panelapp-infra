#!/usr/bin/env python3

import argparse
import collections
import datetime
import difflib
import json
import os
import string
import sys
import textwrap
from typing import Iterable, Tuple, cast

import requests
from tabulate import tabulate

JIRA_TOKEN = os.environ["JIRA_TOKEN"]
HEADERS = {
    "Accept": "application/json",
    "Authorization": f"Bearer {JIRA_TOKEN}",
}
VULNERABILITY_SCORE = {
    "critical": {"score": 9.0, "due": datetime.timedelta(days=14)},
    "high": {"score": 7.0, "due": datetime.timedelta(days=14)},
    "medium": {"score": 4.0, "due": datetime.timedelta(days=90)},
    "low": {"score": 0.1, "due": datetime.timedelta(days=3650)},
}
SCORES = VULNERABILITY_SCORE.keys()

DISPLAY_RESOURCE_LIMIT = 30

APP_TITLE = os.environ.get("APP_TITLE", "Security Fix")
APP_NAME = os.environ.get("APP_NAME", "unknown")

JIRA_PROJECT_KEY = os.environ.get("JIRA_PROJECT_KEY")
JIRA_EPIC_TITLE_PATTERN = os.environ.get("JIRA_EPIC_TITLE_PATTERN", "Security Fixes")
JIRA_COMPONENT = os.environ.get("JIRA_COMPONENT")
JIRA_EPIC_COMPONENTS = [
    comp
    for x in os.environ.get("JIRA_EPIC_COMPONENTS", "").split(",")
    if (comp := x.strip())
]
JIRA_CUSTOM_FIELDS = {
    key: os.environ.get(f"JIRA_CUSTOM_FIELD_{key.upper()}")
    for key in [
        "acceptance_criteria",
        "story_points",
        "sprint",
        "epic",
        "epic_name",
    ]
}
JIRA_SPRINT_ID = os.environ.get("JIRA_SPRINT_ID", "").strip()
JIRA_TRANSITIONS_TO_READY = [
    x
    for x in os.environ.get("JIRA_TRANSITIONS_TO_DESIRED_STATE", "")
    .replace(",", " ")
    .split()
]
JIRA_EPIC_TRANSITIONS_TO_READY = [
    x
    for x in os.environ.get("JIRA_EPIC_TRANSITIONS_TO_DESIRED_STATE", "")
    .replace(",", " ")
    .split()
]
JIRA_CLOSED_STATUSES = ["Discarded", "Done"]
JIRA_IGNORED_DUPLICATE_TYPES = ["Cyber Security Exception", "Epic"]
JIRA_PRIORITY = {
    "critical": {"id": "1"},
    "high": {"id": "2"},
    "medium": {"id": "3"},
    "low": {"id": "4"},
}

SUPPRESSION_LIST = [
    stripped
    for x in os.environ.get("SUPPRESSION_LIST", "").split("\n")
    if (stripped := x.strip())
]

errors = False
EX_ERR = 1  # generic error


def parse_args(argv) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        metavar="COMMAND",
        dest="command",
        required=True,
        help=f"help with: {sys.argv[0]} COMMAND --help",
    )
    parser_attach = subparsers.add_parser(
        "attach", help="attach files to existing Jira issue"
    )
    parser_attach.add_argument("jira", metavar="JIRA_ID", help="Jira issue id")
    parser_attach.add_argument(
        "files",
        metavar="FILE",
        nargs="+",
        default=[],
        help="one or more files to attach",
    )

    parser_create = subparsers.add_parser("create", help="create a new security ticket")
    parser_create.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="do not create Jira tickets",
    )
    parser_create.add_argument(
        "--verbose", action="store_true", default=False, help="print payload"
    )
    parser_create.add_argument(
        "--label",
        metavar="LABEL",
        dest="labels",
        default=[],
        action="append",
        help="add label to Jira ticket",
    )
    parser_create.add_argument(
        "--min-score",
        metavar="SCORE",
        type=float,
        default=0.1,
        help="do not create tickets below SCORE (default: %(default)s)",
    )
    parser_create.add_argument(
        "reports", metavar="REPORT", nargs="+", default=[], help="path to JSON report"
    )

    parser_get = subparsers.add_parser("get", help="get ticket as JSON")
    parser_get.add_argument("jira", metavar="JIRA_ID", help="Jira issue id")

    # parser_epic = \
    subparsers.add_parser("epic", help="get this quarter's epic")

    parser_test = subparsers.add_parser("test", help="create a test ticket")
    parser_test.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        help="do not create Jira tickets",
    )
    parser_test.add_argument(
        "--verbose", action="store_true", default=False, help="print payload"
    )
    parser_test.add_argument(
        "--epic", action="store_true", default=False, help="create a test epic"
    )

    parser_summary = subparsers.add_parser(
        "stats", help="create a statistics/summary report"
    )
    parser_summary.add_argument(
        "--by-resources",
        action="store_true",
        default=False,
        help="print summary by resources",
    )
    parser_summary.add_argument(
        "reports", metavar="REPORT", nargs="+", default=[], help="path to JSON report"
    )
    return parser.parse_args(argv)


def url(path: str) -> str:
    return f"https://jira.extge.co.uk/rest/api/2/{path}"


def summary(args: argparse.Namespace, vuln: dict) -> str:
    """Summary used for all security tickets"""
    return f"{APP_TITLE}: {vuln['id']}"


def epic_name() -> str:
    """Epic (summary) used for all security tickets"""
    this_year = datetime.date.today().year
    this_month = datetime.date.today().month
    first_year, quarter = calendar_month_to_financial_qarter(this_year, this_month)
    format_keys = dict(
        Q=str(quarter),
        YY1=str(first_year)[2:],
        YYYY1=str(first_year),
        YY2=str(first_year + 1)[2:],
        YYYY2=str(first_year + 1),
    )
    epic_name_ = JIRA_EPIC_TITLE_PATTERN
    for key, value in format_keys.items():
        epic_name_ = epic_name_.replace(f"{{{key}}}", value)
    return epic_name_


def calendar_month_to_financial_qarter(year: int, month: int) -> Tuple[int, int]:
    """Returns the first year and quarter of the finacial year for the calendar (year, month).

    >>> calendar_month_to_financial_qarter(2024, 1)
    (2023, 4)
    """
    quarter = ((month + 2) // 3 - 1) or 4
    if quarter == 4:
        return year - 1, quarter
    else:
        return year, quarter


def jql_quote(text: str) -> str:
    return text.replace('"', r"\"")


def get_tickets_by_summary(args, summary: str) -> list:
    global errors
    query = {
        "jql": f'project = {JIRA_PROJECT_KEY} AND summary ~ "{jql_quote(summary)}"',
        "fields": "description,summary,status,issuetype",
    }

    response = requests.request("GET", url("search"), headers=HEADERS, params=query)
    if response.status_code == requests.codes.ok:
        return [
            issue
            for issue in response.json()["issues"]
            if issue.get("fields", {}).get("summary") == summary
        ]
    else:
        print(
            f"no issues found: query={query}, status={response.status_code}",
            file=sys.stderr,
        )
        errors = True
        return []


def get_security_epic(args: argparse.Namespace) -> str | None:
    epic = epic_name()
    tickets = get_tickets_by_summary(args, epic)
    if not tickets:
        epic_key = _create_epic(args, epic)
        return epic_key
    elif len(tickets) == 1:
        return tickets[0]["key"]
    if len(tickets) > 1:
        print(f'Epic "{epic}" not unique', file=sys.stderr)
        print(
            "\n".join([f"  - {ticket['key']}" for ticket in tickets]), file=sys.stderr
        )
    return None


def get_ticket_by_id(args) -> dict:
    query = {
        "jql": f"key = {args.jira}",
    }

    response = requests.request("GET", url("search"), headers=HEADERS, params=query)
    if response.status_code == requests.codes.ok:
        return response.json()["issues"]
    else:
        print(f"{response.status_code}: {response.reason}", file=sys.stderr)
        print(response.json(), file=sys.stderr)
        sys.exit(os.EX_UNAVAILABLE)


def _create_ticket(args: argparse.Namespace, obj: dict) -> None:
    special_headers = {"Content-Type": "application/json"}
    labels = {"security", "automatic"}
    labels.update(args.labels)
    if "image_tag" in obj:
        labels.add("ecr")
    if "scanner" in obj:
        scanner = obj["scanner"]
        if not scanner.startswith("AWS"):
            scanner = f"AWS {scanner}"
        labels.add(scanner)
    payload = {
        "fields": {
            "project": {"key": JIRA_PROJECT_KEY},
            "summary": summary(args, obj),
            "description": format_description_jira(obj),
            "components": [{"name": JIRA_COMPONENT}],
            "issuetype": {"name": "Task"},
            "labels": sorted(label.lower().replace(" ", "-") for label in labels),
            JIRA_CUSTOM_FIELDS["acceptance_criteria"]: [
                {"id": 1, "rank": 0, "name": "All affected resources are known"},
                {"id": 2, "rank": 1, "name": "The root cause is identified"},
                {
                    "id": 3,
                    "rank": 2,
                    "name": "The vulnerability is fixed or an exception is approved",
                    "isHeader": True,
                },
                {"id": 4, "rank": 3, "name": "DEV"},
                {"id": 5, "rank": 4, "name": "TEST"},
                {"id": 6, "rank": 5, "name": "E2E"},
                {"id": 7, "rank": 6, "name": "UAT"},
                {"id": 8, "rank": 7, "name": "PROD"},
            ],
            JIRA_CUSTOM_FIELDS["story_points"]: 2.0,
            JIRA_CUSTOM_FIELDS["epic"]: get_security_epic(args),
        }
    }
    if JIRA_SPRINT_ID:
        payload["fields"][JIRA_CUSTOM_FIELDS["sprint"]] = int(JIRA_SPRINT_ID)

    score = obj["score"]
    base_date = datetime.date.today()
    if obj.get("first_observed_at"):
        base_date = datetime.datetime.fromisoformat(obj["first_observed_at"]).date()
    if score >= VULNERABILITY_SCORE["critical"]["score"]:
        payload["fields"]["priority"] = JIRA_PRIORITY["critical"]
        payload["fields"]["duedate"] = (
            base_date + cast(datetime.timedelta, VULNERABILITY_SCORE["critical"]["due"])
        ).isoformat()
    elif score >= VULNERABILITY_SCORE["high"]["score"]:
        payload["fields"]["priority"] = JIRA_PRIORITY["high"]
        payload["fields"]["duedate"] = (
            base_date + cast(datetime.timedelta, VULNERABILITY_SCORE["high"]["due"])
        ).isoformat()
    elif score >= VULNERABILITY_SCORE["medium"]["score"]:
        payload["fields"]["priority"] = JIRA_PRIORITY["medium"]
        payload["fields"]["duedate"] = (
            base_date + cast(datetime.timedelta, VULNERABILITY_SCORE["medium"]["due"])
        ).isoformat()
    else:
        payload["fields"]["priority"] = JIRA_PRIORITY["low"]

    if args.verbose:
        print(json.dumps(payload, sort_keys=True, indent=4, separators=(",", ": ")))
    if args.dry_run:
        return

    response = requests.request(
        "POST",
        url("issue"),
        headers=dict(**HEADERS, **special_headers),
        data=json.dumps(payload),
    )
    if response.status_code != requests.codes.created:
        print(f"{response.status_code}: {response.reason}", file=sys.stderr)
        print(response.json())
    else:
        print(f"ticket created: {response.json()['key']}")
        change_ticket_to_ready(response.json()["key"], JIRA_TRANSITIONS_TO_READY)


def _create_epic(args: argparse.Namespace, title: str) -> str | None:
    special_headers = {"Content-Type": "application/json"}
    description = textwrap.dedent(
        """\
        h3. Context and Value Delivered
        Genomics England has a layered defense in depth approach to security. One of the most important layers or
        pillars is a well patched and configured system. Good internal hygiene can prevent, or at least makes it a lot
        harder for an attacker to breach systems or move laterally if they do get a foothold. This pillar of security
        is highlighted in many security standards and frameworks such as NIST 800, NIST CSF, Cyber Essentials and the
        National Cyber Security Center Cyber Assurance Framework (NCSC CAF) all of which heavily influence the Genomics
        England IT Security Policy.

        h3. In Scope
         * Capturing and prioritising security vulnerabilities.
         * Implementing fixes and patches to address identified security vulnerabilities.

        h3. Out of Scope
         * Major architectural changes or redesigns. Those should be captured in separate Epics.

        h3. Acceptance Criteria
         * All identified security vulnerabilities are analysed, documented, and prioritized based on their severity and
           potential impact.

        h3. Risks and Assumptions
         * Risk: Some security vulnerabilities may be complex or difficult to address, requiring additional time and
           resources for remediation.

        h3. Notes
         * [POL-IGS-004 Genomics England IT Security Policy|https://cnfl.extge.co.uk/display/public/GEQMS/POL-IGS-004+Genomics+England+IT+Security+Policy]
         * [POL-IGS-006 Vulnerability and Technical Assurance Policy|https://cnfl.extge.co.uk/display/public/GEQMS/POL-IGS-006+Vulnerability+and+Technical+Assurance+Policy]
         * [GUI-IGS-007 - Cyber Security Exception Process Guidance|https://cnfl.extge.co.uk/display/public/GEQMS/GUI-IGS-007+-+Cyber+Security+Exception+Process+Guidance]
         * [Vulnerabilities against 14 day policy|https://analytics.genomicsengland.co.uk/#/views/AWSSecurityReporting/Over14DayoldVulnerabilities?:iid=1]
         * [Vulnerability Management Guidelines|https://cnfl.extge.co.uk/display/security/Vulnerability+Management+Guidelines]
    """  # noqa: E501
    )
    payload = {
        "fields": {
            "project": {"key": JIRA_PROJECT_KEY},
            "summary": title,
            "description": description,
            "components": [{"name": x} for x in JIRA_EPIC_COMPONENTS],
            "issuetype": {"name": "Epic"},
            "labels": ["security", "automatic"],
            JIRA_CUSTOM_FIELDS["epic_name"]: title,
        }
    }

    if args.verbose:
        print(payload)
        print(json.dumps(payload, sort_keys=True, indent=4, separators=(",", ": ")))
    if args.dry_run:
        return None

    response = requests.request(
        "POST",
        url("issue"),
        headers=dict(**HEADERS, **special_headers),
        data=json.dumps(payload),
    )
    if response.status_code != requests.codes.created:
        print(f"{response.status_code}: {response.reason}", file=sys.stderr)
        print(response.json())
    else:
        key = response.json()["key"]
        print(f"epic created: {key}")
        change_ticket_to_ready(key, JIRA_EPIC_TRANSITIONS_TO_READY)
        return key
    return None


def change_ticket_to_ready(ticket_id: str, transitions: Iterable[str]) -> None:
    special_headers = {"Content-Type": "application/json"}

    def transition(transition_id):
        payload = json.dumps({"transition": {"id": transition_id}})
        response = requests.request(
            "POST",
            url(f"issue/{ticket_id}/transitions"),
            headers=dict(**HEADERS, **special_headers),
            data=payload,
        )
        if response.status_code != requests.codes.no_content:
            print(f"{response.status_code}: {response.reason}", file=sys.stderr)
            print(response.json())

    for trans in transitions:
        transition(trans)


def format_description_jira(obj) -> str:
    description = textwrap.dedent(
        f"""\
        h3. Summary
        Vulnerability: {obj['id']}
        Severity: {obj['severity']}
        Score: {obj['score']}
        """
    )
    if obj.get("packages"):
        description += textwrap.dedent(
            """
            h3. Affected Packages
            """
        )
        for pkg in obj["packages"]:
            description += f" * {pkg['name']} {pkg['version']}\n"
        description += "\n"
    description += textwrap.dedent(
        """
        h3. Affected Resources
        """
    )
    resources = set()
    for rsc in obj["resources"]:
        account = rsc["account"]
        if rsc["type"] == "AWS_ECR_CONTAINER_IMAGE":
            tags = ("ci-", "build-", "dev-", "test-", "e2e-", "uat-", "prod-")
            resources.add(
                f"{account['name']} ({account['id']}): {rsc['type']} "
                f"{', '.join(t for t in rsc['tags'] if not t.startswith(tags))}"
            )
        else:
            resources.add(
                f"{account['name']} ({account['id']}): {rsc['type']} {rsc['id']}"
            )
    for resource in sorted(resources)[:DISPLAY_RESOURCE_LIMIT]:
        description += f" * {resource}\n"
    if len(resources) > DISPLAY_RESOURCE_LIMIT:
        description += f"\n... (total: {len(resources)})\n"

    description += textwrap.dedent(
        """

        h3. Description
        """
    )
    safe_chars = (
        string.ascii_letters
        + string.digits
        + string.whitespace
        + """#$%'()*+,-./:;<=>?[]^_\""""
    )
    cleared = "".join(c for c in obj["description"] if c in safe_chars)
    description += cleared
    if cleared != obj["description"]:
        description += "\n\n[some characters were removed for interoperability]"
    if obj.get("first_observed_at"):
        timestamp = datetime.datetime.fromisoformat(obj["first_observed_at"]).isoformat(
            timespec="minutes"
        )
        description += textwrap.dedent(
            f"""


            h3. Timeline
            The vulnerability was first observed at: {timestamp}
        """
        )
    description += textwrap.dedent(
        """

        h3. IMPORTANT
        This is an automatically generated Jira issue.
        DO NOT change the TITLE, or CLOSE the issue before the underlying problem is fixed.
        Otherwise it may be re-created again.
    """
    )

    if cleared != obj["description"]:
        print("Characters removed for interoperability:")
        print(
            "".join(
                difflib.ndiff(
                    cleared.splitlines(keepends=True),
                    obj["description"].splitlines(keepends=True),
                )
            )
        )
    return description


def command_create(args: argparse.Namespace) -> None:
    errors = []
    vulnerabilities = read_reports(args)
    for vuln_id, vuln in vulnerabilities.items():
        try:
            if vuln_id in SUPPRESSION_LIST:
                print(f"suppressed: {vuln_id}")
                continue
            all_tickets = get_tickets_by_summary(args, summary(args, vuln))
            open_tickets = [
                t
                for t in all_tickets
                if t["fields"]["status"]["name"] not in JIRA_CLOSED_STATUSES
                and t["fields"]["issuetype"]["name"] not in JIRA_IGNORED_DUPLICATE_TYPES
            ]
            if open_tickets:
                print(f"ticket already exists: {vuln_id}:")
                for t in open_tickets:
                    print(f"  - {t['key']}: {t['fields']['summary']}")
            else:
                if vuln["score"] < args.min_score:
                    print(f"skipped: score below threshold: {vuln_id}")
                    continue
                print(f"creating ticket for {vuln_id}")
                _create_ticket(args, vuln)
        except Exception as exc:
            errors.append(f"{exc.__class__.__name__}: {str(exc)}")

    print_impact_report(vulnerabilities)

    if errors:
        print("\n".join(errors), file=sys.stderr)
        sys.exit(os.EX_CANTCREAT)


def command_test(args: argparse.Namespace) -> None:
    vulnerabilities = {
        "TEST: Security scan Jira integration (IGNORE)": {
            "id": "TEST: Security scan Jira integration (IGNORE)",
            "severity": "MEDIUM",
            "score": 4.5,
            "resources": [
                {
                    "account": {"id": "1234567890", "name": "test_dev"},
                    "type": "OTHER",
                    "id": "arn:of:something",
                }
            ],
            "description": "Security scan Jira integration",
        }
    }
    if args.epic:
        _create_epic(args, f"TEST (IGNORE): {epic_name()}")
    else:
        for vuln in vulnerabilities.values():
            _create_ticket(args, vuln)
        print_impact_report(vulnerabilities)


def print_impact_report(vulns: dict) -> None:
    print("\n\n\n")
    print("Vulnerability impact:")
    for vuln_id, vuln in vulns.items():
        print(f"{vuln_id}:")
        resources = set()
        for res in vuln["resources"]:
            resources.add(
                f"  - {res['account']['name']}: {res['type']}: {res['tags'] if 'tags' in res else res['id']}"
            )
        for line in sorted(resources)[:DISPLAY_RESOURCE_LIMIT]:
            print(line)
        if len(resources) > DISPLAY_RESOURCE_LIMIT:
            print(f"  ... (total: {len(resources)})")


def read_reports(args: argparse.Namespace) -> dict:
    global errors
    vulnerabilities = {}
    for path in args.reports:
        try:
            data = json.load(open(path))
        except (FileNotFoundError, json.decoder.JSONDecodeError) as exc:
            print(f"{exc.__class__.__name__}: {str(exc)}: {path}", file=sys.stderr)
            errors = True
            continue
        else:
            for vuln in data["findings"]:
                if vuln["id"] not in vulnerabilities:
                    vulnerabilities[vuln["id"]] = vuln
                else:
                    vulnerabilities[vuln["id"]]["resources"] += vuln["resources"]
    return vulnerabilities


def command_stats(args: argparse.Namespace) -> None:
    vulnerabilities = read_reports(args)
    counter = collections.Counter()  # type: collections.Counter
    res_counter = collections.Counter()  # type: collections.Counter
    for vuln_id, vuln in vulnerabilities.items():
        for score in SCORES:
            if vuln["score"] >= VULNERABILITY_SCORE[score]["score"]:
                per_vuln_counter = collections.Counter()  # type: collections.Counter
                for res in vuln["resources"]:
                    account_name = res["account"]["name"]
                    per_vuln_counter[f"{score}:{account_name}"] += 1
                for key, value in per_vuln_counter.items():
                    res_counter[key] += value
                    counter[key] += 1
                break

    envs = ("dev", "test", "e2e", "uat", "prod")
    now = datetime.datetime.now(datetime.timezone.utc).isoformat(
        sep=" ", timespec="minutes"
    )

    if args.by_resources:
        header = f"Vulnerable resources per account ({now})"
        table = [
            [score.upper(), *[res_counter[f"{score}:{APP_NAME}_{env}"] for env in envs]]
            for score in SCORES
        ]
        footer = (
            "Vulnerabilities with several affected resources are counted multiple times"
        )
    else:
        header = f"Vulnerabilities per account ({now})"
        table = [
            [score.upper(), *[counter[f"{score}:{APP_NAME}_{env}"] for env in envs]]
            for score in SCORES
        ]
        footer = (
            "Vulnerabilities with multiple affected resources are counted only once"
        )
    print(header)
    print(tabulate(table, headers=[APP_TITLE, *envs], tablefmt="fancy_grid"))
    print(footer)


def command_attach(args: argparse.Namespace) -> None:
    special_headers = {"X-Atlassian-Token": "no-check"}
    for path in args.files:
        response = requests.request(
            "POST",
            url(f"issue/{args.jira}/attachments"),
            headers=dict(**HEADERS, **special_headers),
            files={"file": (os.path.basename(path), open(path, "rb"), "text/plain")},
        )
        if response.status_code != requests.codes.ok:
            print(response, file=sys.stderr)
            sys.exit(os.EX_IOERR)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    print(args, file=sys.stderr)
    if args.command == "attach":
        command_attach(args)
    elif args.command == "create":
        command_create(args)
    elif args.command == "test":
        command_test(args)
    elif args.command == "stats":
        command_stats(args)
    elif args.command == "get":
        print(
            json.dumps(
                get_ticket_by_id(args), sort_keys=True, indent=4, separators=(",", ": ")
            )
        )
    elif args.command == "epic":
        print(get_security_epic(args))

    if errors:
        sys.exit(EX_ERR)
