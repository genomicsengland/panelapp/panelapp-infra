
all: test

venv:
	python3 -m venv venv
	. venv/bin/activate && pip install pre-commit boto3-stubs types-requests types-tabulate
	. venv/bin/activate && pre-commit install-hooks

test: venv
	. venv/bin/activate && pre-commit run --all-files

test-all: venv
	. venv/bin/activate && pre-commit run --hook-stage pre-commit --hook-stage manual --all-files


install: venv
	. venv/bin/activate && pre-commit install

clean:
	. venv/bin/activate && pre-commit uninstall
	. venv/bin/activate && pre-commit gc
	rm -rf venv

.PHONY: all test test-all install clean
