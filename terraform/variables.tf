variable "region" {
  type        = string
  description = "AWS Region"
  default     = "eu-west-2"
}

variable "env_name" {
  type        = string
  description = "Environment name"
}

variable "account_id" {
  type        = string
  description = "Account ID"
}

# Fargate/Application
variable "panelapp_replica" {
  type    = number
  default = 2
}

variable "image_tag" {
  type        = string
  description = "application image version to deploy"
  default     = ""
}

variable "infra_tag" {
  type        = string
  description = "infrastructure version to deploy (informational only)"
  default     = ""
}

variable "aurora" {
  type = object({
    cluster_size               = number
    instance_class             = string
    engine_version             = string
    auto_minor_version_upgrade = bool
  })
  default = {
    cluster_size               = 1
    instance_class             = "db.t3.medium"
    engine_version             = "14"
    auto_minor_version_upgrade = true
  }
}

variable "session_cookie_age" {
  type        = number
  default     = 28800
  description = "Session cookie age in seconds; default is 8 hours"
}

variable "desired_task_count" {
  type = object({
    web    = number
    worker = number
  })
  default = {
    web    = 4
    worker = 2
  }
}

variable "gunicorn_workers" {
  type    = number
  default = 2
}

variable "gunicorn_accesslog" {
  type        = string
  description = "The Access log file to write to - means log to stdout"
  default     = "-"
}

variable "application_connection_timeout" {
  type    = number
  default = 300
}

variable "create_mgmt_box" {
  type    = bool
  default = false
}

variable "snapshot_identifier" {
  type    = string
  default = ""
}

variable "restore_from_snapshot" {
  type        = bool
  description = "no longer used"
  default     = false
}

variable "active_scheduled_tasks" {
  type        = list(string)
  description = "list of periodic celery beat tasks"
  default     = []
}

variable "signed_off_archive_base_url" {
  type        = string
  description = "Signed off panels static site"
  default     = ""
}

variable "vpc_id" {
  type        = string
  description = "Network configuration"
}

variable "aws_backup_enabled" {
  type        = bool
  default     = true
  description = "Should the Backup module be used, you can use this switch to disable backup on certain envs"
}

variable "backup_vault_lock_enabled" {
  type        = bool
  description = "Whether or not to enable the vault lock"
  default     = false
}

variable "backup_plans" {
  type = list(object({
    name              = string
    schedule          = string
    delete            = number
    start_window      = number
    completion_window = number
  }))
  description = "List of maps with settings of backup plans for non-prod"
  default = [
    { name = "panelapp_daily", schedule = "cron(0 4 * * ? *)", delete = 30, start_window = 60, completion_window = 300 },
    { name = "panelapp_weekly", schedule = "cron(0 4 ? * 1 *)", delete = 180, start_window = 60, completion_window = 300 },
    { name = "panelapp_monthly", schedule = "cron(0 4 1 * ? *)", delete = 180, start_window = 60, completion_window = 300 },
  ]
}

variable "side_cars" {
  description = "Repository:version of side cars from core_bakery"
  type = object({
    datadog_agent = string
    fluentbit     = string
  })
  default = {
    datadog_agent = "latest version"
    fluentbit     = "latest version"
  }
}
