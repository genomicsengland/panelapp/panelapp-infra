env_name   = "test"
account_id = "455662437776"
vpc_id     = "vpc-02368dae78f1387e5"

aurora = {
  cluster_size               = 1
  instance_class             = "db.t3.medium"
  engine_version             = "14"
  auto_minor_version_upgrade = true
}

signed_off_archive_base_url = "https://test-nhsgms-panelapp.genomicsengland.co.uk"
active_scheduled_tasks = [
  "moi-check",
]

create_mgmt_box = false
