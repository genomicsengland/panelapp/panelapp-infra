module "datadog-integration" {
  count  = local.is_default_ws_count
  source = "./modules/datadog_integration"
}

module "datadog" {
  source = "./modules/datadog"

  application_version = var.image_tag
  ecs_services        = module.services.services
  env_name            = var.env_name
  #FIXME: split into two monitors, one for aws.gel.ac and one for genomicsengland.co.uk
  cdn_alias        = local.is_default_ws ? "${local.dns.app.external}.${local.dns.zone.external}" : "${local.dns.app.internal}.${local.dns.zone.internal}"
  media_cdn_alias  = "${local.dns.media.external}.${local.dns.zone.external}"
  static_cdn_alias = "${local.dns.static.external}.${local.dns.zone.external}"
  tags             = local.datadog_tags
}
