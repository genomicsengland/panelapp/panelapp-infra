locals {
  is_registry_builder = (var.account_id == local.ecr_registry_account_id) && local.is_default_ws
}

module "ecr" {
  count  = local.is_registry_builder ? 1 : 0
  source = "./modules/ecr"

  repo_names              = ["panelapp"]
  backup_tags             = local.backup_tags
  kms_key_arn             = local.kms_key_arn
  permissions_boundary    = local.permissions_boundary
  pull_trusted_accounts   = local.ecr_pull_trusted_accounts
  push_pull_trusted_roles = local.ecr_push_pull_trusted_roles
}
