module "aws-backup" {
  source                   = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/aws-backup.git?ref=75c0f0627e1bf205fc1a55b41af0f94c38db4581" # commit hash of version: v2.0.3
  count                    = (var.aws_backup_enabled && local.is_default_ws) ? 1 : 0
  default_tags             = module.aws_tags.tags
  resource_prefix          = "${module.aws_tags.service}-${var.env_name}"
  vault_encryption_kms_key = local.kms_key_arn
  region                   = var.region
  iam_boundary             = local.permissions_boundary

  backup_plans         = var.backup_plans
  backup_plan_tag_name = ["aws_backup_plan_daily", "aws_backup_plan_weekly", "aws_backup_plan_monthly"]
  backup_notify_events = ["BACKUP_JOB_STARTED", "BACKUP_JOB_FAILED", "BACKUP_JOB_COMPLETED", "RESTORE_JOB_STARTED", "RESTORE_JOB_COMPLETED"]
  vault_lock_enabled   = var.backup_vault_lock_enabled
  enable_s3_backups    = true

  vault_policy_allow = [
    {
      arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
      allow = [
        "backup:DescribeBackupVault",
        "backup:GetBackupVaultNotifications",
        "backup:ListRecoveryPointsByBackupVault"
      ]
    },
    {
      arn = local.admin_access,
      allow = [
        "backup:StartBackupJob",
        "backup:GetRecoveryPointRestoreMetadata",
        "backup:DescribeBackupVault",
        "backup:GetBackupVaultAccessPolicy",
        "backup:GetBackupVaultNotifications",
        "backup:PutBackupVaultNotifications"
      ],
    },
  ]

  vault_policy_deny = [
    {
      arn = "*",
      deny = [
        "backup:DeleteBackupVault",
      ]
    }
  ]

  snapshot_policy_allow = [
    {
      arn = local.admin_access,
      allow = [
        "backup:StartRestoreJob",
        "backup:DescribeRecoveryPoint"
      ],
    },
  ]

  snapshot_policy_deny = [
    {
      arn = "*",
      deny = [
        "backup:DeleteRecoveryPoint",
      ]
    }
  ]
}

resource "aws_sns_topic_subscription" "backup_notifications" {
  count     = (var.aws_backup_enabled && local.is_default_ws) ? 1 : 0
  topic_arn = module.aws-backup[count.index].backup_sns_topic_arn
  protocol  = "https"
  endpoint  = "https://app.datadoghq.eu/intake/webhook/sns?api_key=${data.aws_secretsmanager_secret_version.datadog_api_key.secret_string}"
}
