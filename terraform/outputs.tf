output "db_host" {
  value = module.aurora.writer_endpoint
}

output "db_port" {
  value = module.aurora.port
}

output "db_name" {
  value = module.aurora.database_name
}

output "statics_bucket" {
  value = local.cdn_buckets.statics.name
}

output "media_bucket" {
  value = local.cdn_buckets.media.name
}

output "ecs_cluster" {
  value = module.services.cluster.name
}

output "automation" {
  value = {
    standalone_tasks = module.automation.ssm_documents.standalone_tasks.name
  }
}

output "ssm_parameters" {
  value = {
    rds_snapshot = module.aurora.rds_snapshot_ssm_parameter
  }
}

output "shared_secrets" {
  value = {
    cloudflare_api_key = module.cloudflare.api_key
  }
}
output "cloudflare" {
  value = {
    hosts = [
      "${local.dns.app.external}.${local.dns.zone.external}",
      "${local.dns.media.external}.${local.dns.zone.external}",
      "${local.dns.static.external}.${local.dns.zone.external}",
    ]
  }
}

output "snapshot" {
  value = {
    name = replace(var.snapshot_identifier, "/.*:/", "")
  }
}
