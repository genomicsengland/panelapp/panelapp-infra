locals {
  # System and Runbook Automation / DR tests; potentially destructive, restricts permissions.
  is_throwaway_env = (var.env_name == "dev" || var.env_name == "test")
}

resource "aws_iam_role" "testing" {
  # Role name is referenced in https://gitlab.com/genomicsengland/cloud/aws-core/shared/gitlab-runner
  # and https://gitlab.com/genomicsengland/kmds-validation/kmds_automation_tests
  name                 = "Testing${local.is_default_ws ? "" : "-${local.name.ws_product}"}"
  permissions_boundary = local.permissions_boundary
  assume_role_policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Sid    = "AssumeRoleForGitlab"
          Effect = "Allow"
          Principal = {
            AWS = [
              "arn:aws:iam::512426816668:role/GitLabRunnerWorker_kmds_dev",
              "arn:aws:iam::512426816668:role/GitLabRunnerWorker_kmds_test",
              "arn:aws:iam::512426816668:role/GitLabRunnerWorker_kmds_uat",
              "arn:aws:iam::512426816668:role/GitLabRunnerWorker_kmds_prod",
            ]
          }
          Action = "sts:AssumeRole"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "testing" {
  policy_arn = aws_iam_policy.testing.arn
  role       = aws_iam_role.testing.name
}

resource "aws_iam_policy" "testing" {
  name = "${local.name.ws_product}-testing"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid    = "RunAutomation"
        Effect = local.is_throwaway_env ? "Allow" : "Deny"
        Action = [
          "ssm:StartAutomationExecution"
        ],
        Resource = [
          for document in module.automation.ssm_documents : "${replace(document.arn, ":document/", ":automation-definition/")}:*"
        ]
      },
      {
        Sid    = "WaitForAutomation"
        Effect = "Allow"
        Action = [
          "ssm:DescribeAutomationExecutions"
        ]
        Resource = [
          "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
        ]
      },
      {
        Sid    = "ReadWriteSSMParameter"
        Effect = "Allow"
        Action = [
          "ssm:GetParameter",
          "ssm:PutParameter"
        ]
        Resource = [module.services.ssm_parameters.panelapp_banner.arn]
      },
      {
        Sid    = "UpdateServices"
        Effect = local.is_throwaway_env ? "Allow" : "Deny"
        Action = [
          "ecs:UpdateService",
          "ecs:DescribeServices",
        ]
        Resource = [for x in module.services.services : x.arn]
      },
      {
        Sid    = "UploadData"
        Effect = "Allow"
        Action = [
          "s3:PutObject"
        ]
        Resource = [
          "${module.automation.buckets.upload.arn}/*",
        ]
      },
      {
        Sid    = "ReadData"
        Effect = "Allow"
        Action = [
          "s3:ListBucket",
          "s3:GetObject"
        ]
        Resource = [
          module.automation.buckets.artifacts.arn,
          "${module.automation.buckets.artifacts.arn}/*",
        ]
      },
      {
        Sid    = "EncryptData"
        Effect = "Allow"
        Action = [
          "kms:GenerateDataKey",
          "kms:Decrypt"
        ]
        Resource = [
          module.automation.kms_keys.automation.key_arn,
          module.automation.kms_keys.automation.alias_arn,
        ]
      },
      {
        Sid    = "DatabaseSnapshots"
        Effect = local.is_throwaway_env ? "Allow" : "Deny"
        Action = [
          "rds:DescribeDBClusterSnapshots",
          "rds:DescribeDBClusterSnapshotAttributes",
          "rds:DeleteDBClusterSnapshot",
        ]
        Resource = [
          module.aurora.aurora_cluster.arn,
          "arn:aws:rds:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:cluster-snapshot:${module.aurora.aurora_cluster.name}-testing-*"
        ]
      }
    ]
  })
}
