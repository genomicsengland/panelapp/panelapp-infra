module "services" {
  source = "./modules/services"

  env_name         = var.env_name
  name             = local.name
  vpc_id           = var.vpc_id
  datadog_tags_map = local.datadog_tags

  certificate_arn  = module.acm_gel.regional_cert
  cdn_alias        = "${local.dns.app.external}.${local.dns.zone.external}"
  media_cdn_alias  = "${local.dns.media.external}.${local.dns.zone.external}"
  static_cdn_alias = "${local.dns.static.external}.${local.dns.zone.external}"
  gmspanels_url    = var.signed_off_archive_base_url

  docker_image = "${local.ecr_registry}/panelapp:${var.image_tag}"
  side_cars    = var.side_cars
  kms_key_arn  = local.kms_key_arn

  database = {
    writer_endpoint     = module.aurora.writer_endpoint
    port                = module.aurora.port
    name                = module.aurora.database_name
    user                = module.aurora.database_user
    master_password_arn = local.aws_ssm_parameter_aurora_master_password.arn
  }

  session = {
    cookie_age = var.session_cookie_age
  }

  email = {
    sender_address  = local.email.sender
    contact_address = local.email.contact
    smtp_server     = "email-smtp.eu-west-2.amazonaws.com"
    smtp_port       = 587
  }

  django = {
    settings_module = local.django_settings_module
    log_level       = "INFO"
    admin_email     = "test@test.com"
  }

  panelapp = {
    tasks = {
      web    = var.desired_task_count.web
      worker = var.desired_task_count.worker
    }
    workers            = var.gunicorn_workers
    connection_timeout = var.application_connection_timeout
    access_log         = var.gunicorn_accesslog
  }

  scheduled_tasks = {
    tasks = var.active_scheduled_tasks
    config = {
      moi_check_day_of_week = "0"
    }
  }

  buckets = merge(local.cdn_buckets, module.automation.buckets)

  providers = {
    aws         = aws
    aws.ssm     = aws.ssm
    aws.secrets = aws.secrets
  }
}

resource "aws_security_group_rule" "fargate_egress_database" {
  type                     = "egress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = module.services.security_group
  source_security_group_id = module.aurora.security_group
  description              = "Allow Postgres to Database"
}
