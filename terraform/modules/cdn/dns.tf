data "aws_route53_zone" "external_domain" {
  provider     = aws.dns
  name         = var.dns.zone.external
  private_zone = false
}

resource "aws_route53_record" "static_external" {
  count    = local.is_default_ws_count
  provider = aws.dns
  zone_id  = data.aws_route53_zone.external_domain.zone_id
  name     = var.dns.static.external
  type     = "CNAME"
  ttl      = 300
  records = [
    "${var.dns.static.external}.${var.dns.zone.external}.cdn.cloudflare.net"
  ]
}

resource "aws_route53_record" "media_external" {
  count    = local.is_default_ws_count
  provider = aws.dns
  zone_id  = data.aws_route53_zone.external_domain.zone_id
  name     = var.dns.media.external
  type     = "CNAME"
  ttl      = 300
  records = [
    "${var.dns.media.external}.${var.dns.zone.external}.cdn.cloudflare.net"
  ]
}
resource "aws_route53_record" "static_external_ws" {
  count    = 1 - local.is_default_ws_count
  provider = aws.dns
  zone_id  = data.aws_route53_zone.external_domain.zone_id
  name     = var.dns.static.external
  type     = "A"

  alias {
    name                   = aws_cloudfront_distribution.panelapp_static_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.panelapp_static_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "media_external_ws" {
  count    = 1 - local.is_default_ws_count
  provider = aws.dns
  zone_id  = data.aws_route53_zone.external_domain.zone_id
  name     = var.dns.media.external
  type     = "A"

  alias {
    name                   = aws_cloudfront_distribution.panelapp_media_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.panelapp_media_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}
