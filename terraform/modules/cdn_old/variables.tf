variable "env_name" {
  type        = string
  description = "environment"
}

variable "dns" {
  description = "CDN alias"
  type = object({
    media_fqdn  = string
    static_fqdn = string
  })
}

variable "certificate_arns" {
  type = object({
    media   = string
    statics = string
  })
}

variable "backup_tags" {
  description = "Tags for automatic backup module"
  type        = map(string)
}
