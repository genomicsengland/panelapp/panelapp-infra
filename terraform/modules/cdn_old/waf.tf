resource "aws_wafv2_ip_set" "cloudflare_ipv4" {
  provider           = aws.us_east_1
  name               = "cloudflareIPSetv4"
  description        = "cloudflareIPSetv4"
  scope              = "CLOUDFRONT"
  ip_address_version = "IPV4"
  addresses          = local.cloudflare_ips.ipv4
}

resource "aws_wafv2_ip_set" "cloudflare_ipv6" {
  provider           = aws.us_east_1
  name               = "cloudflareIPSetv6"
  description        = "cloudflareIPSetv6"
  scope              = "CLOUDFRONT"
  ip_address_version = "IPV6"
  addresses          = local.cloudflare_ips.ipv6
}

resource "aws_wafv2_web_acl" "cloudfront_acl" {
  provider = aws.us_east_1
  name     = "cloudfront-allow-cloudflare-only"

  scope = "CLOUDFRONT"

  default_action {
    block {}
  }

  rule {
    name     = "ip-whitelist-ipv4"
    priority = 1

    action {
      allow {}
    }

    statement {
      ip_set_reference_statement {
        arn = aws_wafv2_ip_set.cloudflare_ipv4.arn
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "WhitelistedIPv4"
      sampled_requests_enabled   = true
    }
  }

  rule {
    name     = "ip-whitelist-ipv6"
    priority = 2

    action {
      allow {}
    }

    statement {
      ip_set_reference_statement {
        arn = aws_wafv2_ip_set.cloudflare_ipv6.arn
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "WhitelistedIPv6"
      sampled_requests_enabled   = true
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "BlockedList"
    sampled_requests_enabled   = true
  }
}
