output "bucket" {
  value = {
    statics = {
      name = aws_s3_bucket.panelapp_statics.id
      arn  = aws_s3_bucket.panelapp_statics.arn
    }
    media = {
      name = aws_s3_bucket.panelapp_media.id
      arn  = aws_s3_bucket.panelapp_media.arn
    }
    scripts = {
      name = aws_s3_bucket.panelapp_scripts.id
      arn  = aws_s3_bucket.panelapp_scripts.arn
    }
  }
}

output "cloudfront_oai" {
  value = aws_cloudfront_origin_access_identity.panelapp_s3.iam_arn
}
