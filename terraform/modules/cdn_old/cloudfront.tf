locals {
  cache_policy_id            = "658327ea-f89d-4fab-a63d-7e88639e58f6"
  origin_request_policy_id   = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  response_headers_policy_id = "eaab4381-ed33-4a86-88ca-d9558dc6cd63"
}

resource "aws_cloudfront_origin_access_identity" "panelapp_s3" {
  comment = "access identity to access s3"
}

resource "aws_cloudfront_distribution" "panelapp_media_distribution" {
  # checkov:skip=CKV2_AWS_32: decommissioned soon
  # FIXME: Phase 2 (downtime)
  aliases = [var.dns.media_fqdn]

  origin {
    domain_name = aws_s3_bucket.panelapp_media.bucket_regional_domain_name
    origin_path = ""
    origin_id   = "S3-panelapp_media"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.panelapp_s3.cloudfront_access_identity_path
    }
  }

  web_acl_id = aws_wafv2_web_acl.cloudfront_acl.arn

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.cloudfront_logs.bucket_domain_name
    # prefix          = "panelapp-cloudfront-logs"
  }

  enabled = true

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    # path_pattern           = "*"
    allowed_methods            = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods             = ["GET", "HEAD"]
    target_origin_id           = "S3-panelapp_media"
    viewer_protocol_policy     = "redirect-to-https"
    cache_policy_id            = local.cache_policy_id
    origin_request_policy_id   = local.origin_request_policy_id
    response_headers_policy_id = local.response_headers_policy_id

  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = var.certificate_arns.media
    ssl_support_method             = "sni-only"
    minimum_protocol_version       = "TLSv1.2_2021"
  }

  custom_error_response {
    error_code            = 403
    response_code         = 404
    error_caching_min_ttl = 60
    response_page_path    = "/media/404.html"
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name = "media_panelapp_cdn"
  }
}

resource "aws_cloudfront_distribution" "panelapp_static_distribution" {
  # checkov:skip=CKV2_AWS_32: decommissioned soon
  # FIXME: Phase 2 (downtime)
  aliases = [var.dns.static_fqdn]

  origin {
    domain_name = aws_s3_bucket.panelapp_statics.bucket_regional_domain_name
    origin_path = ""
    origin_id   = "S3-panelapp_static"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.panelapp_s3.cloudfront_access_identity_path
    }
  }

  web_acl_id = aws_wafv2_web_acl.cloudfront_acl.arn

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.cloudfront_logs.bucket_domain_name
    # prefix          = "panelapp-cloudfront-logs"
  }

  enabled = true

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    # path_pattern           = "*"
    allowed_methods            = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods             = ["GET", "HEAD"]
    target_origin_id           = "S3-panelapp_static"
    viewer_protocol_policy     = "redirect-to-https"
    cache_policy_id            = local.cache_policy_id
    origin_request_policy_id   = local.origin_request_policy_id
    response_headers_policy_id = local.response_headers_policy_id
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn            = var.certificate_arns.statics
    ssl_support_method             = "sni-only"
    minimum_protocol_version       = "TLSv1.2_2021"
  }

  custom_error_response {
    error_code            = 403
    response_code         = 404
    error_caching_min_ttl = 60
    response_page_path    = "/static/404.html"
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name = "static_panelapp_cdn"
  }
}

resource "aws_s3_bucket" "cloudfront_logs" {
  bucket        = "panelapp-${var.env_name}-cloudfront-logs"
  force_destroy = false

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name = "panelapp-${var.env_name}-cloudfront-logs"
  }
}

resource "aws_s3_bucket_public_access_block" "cloudfront_logs" {
  bucket = aws_s3_bucket.cloudfront_logs.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "cloudfront_logs" {
  # checkov:skip=CKV2_AWS_65: decommissioned soon
  bucket = aws_s3_bucket.cloudfront_logs.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_acl" "cloudfront_logs" {
  bucket = aws_s3_bucket.cloudfront_logs.id
  acl    = "log-delivery-write"
  depends_on = [
    aws_s3_bucket_ownership_controls.cloudfront_logs,
    aws_s3_bucket_public_access_block.cloudfront_logs,
  ]
}

resource "aws_s3_bucket_versioning" "cloudfront_logs" {
  bucket = aws_s3_bucket.cloudfront_logs.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "cloudfront_logs" {
  # checkov:skip=CKV_AWS_300: decommissioned soon
  bucket = aws_s3_bucket.cloudfront_logs.id
  rule {
    id     = "expiration"
    status = "Enabled"
    expiration {
      days = 30
    }
    noncurrent_version_expiration {
      noncurrent_days = 1
    }
  }
}

resource "aws_s3_bucket_policy" "cloudfront_logs" {
  bucket = aws_s3_bucket.cloudfront_logs.id
  policy = data.aws_iam_policy_document.cloudfront_logs.json
}

data "aws_iam_policy_document" "cloudfront_logs" {
  statement {
    sid     = "AllowSSLRequestsOnly"
    actions = ["s3:*"]
    effect  = "Deny"
    resources = [
      aws_s3_bucket.cloudfront_logs.arn,
      "${aws_s3_bucket.cloudfront_logs.arn}/*"
    ]
    condition {
      test     = "Bool"
      values   = ["false"]
      variable = "aws:SecureTransport"
    }
    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}
