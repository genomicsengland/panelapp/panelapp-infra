locals {
  bucket_name_prefix = "panelapp-${var.env_name}-${data.aws_caller_identity.current.account_id}-${data.aws_region.current.name}-panelapp"
}

resource "aws_s3_bucket" "panelapp_statics" {
  # checkov:skip=CKV2_AWS_6: decommissioned soon
  bucket = "${local.bucket_name_prefix}-statics"
  tags = {
    Name = "panelapp_static"
  }
}

resource "aws_s3_bucket_versioning" "panelapp_statics" {
  bucket = aws_s3_bucket.panelapp_statics.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "panelapp_statics" {
  bucket = aws_s3_bucket.panelapp_statics.id
  rule {
    id     = "noncurrent"
    status = "Enabled"
    noncurrent_version_expiration {
      noncurrent_days = 30
    }
  }
  rule {
    id     = "delete-marker"
    status = "Enabled"
    expiration {
      expired_object_delete_marker = true
    }
    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "panelapp_statics" {
  bucket = aws_s3_bucket.panelapp_statics.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket" "panelapp_media" {
  # checkov:skip=CKV2_AWS_6: decommissioned soon
  bucket = "${local.bucket_name_prefix}-media"
  tags   = merge({ Name = "panelapp_media" }, var.backup_tags)
}

resource "aws_s3_bucket_server_side_encryption_configuration" "panelapp_media" {
  bucket = aws_s3_bucket.panelapp_media.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "panelapp_media" {
  bucket = aws_s3_bucket.panelapp_media.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "panelapp_media" {
  bucket = aws_s3_bucket.panelapp_media.id
  rule {
    id     = "noncurrent"
    status = "Enabled"
    noncurrent_version_expiration {
      noncurrent_days = 30
    }
  }
  rule {
    id     = "delete-marker"
    status = "Enabled"
    expiration {
      expired_object_delete_marker = true
    }
    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}

resource "aws_s3_bucket_ownership_controls" "panelapp_media" {
  # checkov:skip=CKV2_AWS_6: decommissioned soon
  # checkov:skip=CKV2_AWS_65: decommissioned soon
  bucket = aws_s3_bucket.panelapp_media.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket" "panelapp_scripts" {
  # checkov:skip=CKV2_AWS_6: decommissioned soon
  bucket = "${local.bucket_name_prefix}-scripts"
  tags = {
    Name = "panelapp_scripts"
  }
}

resource "aws_s3_bucket_versioning" "panelapp_scripts" {
  bucket = aws_s3_bucket.panelapp_scripts.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "panelapp_scripts" {
  bucket = aws_s3_bucket.panelapp_scripts.id
  rule {
    id     = "noncurrent"
    status = "Enabled"
    noncurrent_version_expiration {
      noncurrent_days = 30
    }
  }
  rule {
    id     = "delete-marker"
    status = "Enabled"
    expiration {
      expired_object_delete_marker = true
    }
    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
  }
}

resource "aws_s3_bucket_policy" "panelapp_scripts" {
  bucket = aws_s3_bucket.panelapp_scripts.id
  policy = data.aws_iam_policy_document.panelapp_scripts.json
}

data "aws_iam_policy_document" "panelapp_scripts" {
  statement {
    sid     = "AllowSSLRequestsOnly"
    actions = ["s3:*"]
    effect  = "Deny"
    resources = [
      aws_s3_bucket.panelapp_scripts.arn,
      "${aws_s3_bucket.panelapp_scripts.arn}/*"
    ]
    condition {
      test     = "Bool"
      values   = ["false"]
      variable = "aws:SecureTransport"
    }
    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}
