locals {
  permissions_boundary                = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/GELBoundary"
  datadog_aws_integration_external_id = "5900512774f44cbeb9191dbb49fa36c6"
}
