module "datadog-integration" {
  source     = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/datadog-integration?ref=0b4c25c29e60af72b25a86102e04e5f03a9f2362" # commit hash of version: v3.0.1
  account_id = data.aws_caller_identity.current.account_id
  region     = data.aws_region.current.name
  account_specific_datadog_namespace_rules = {
    application_elb        = true
    auto_scaling           = true
    certificatemanager     = true
    cloudfront             = true
    cloudwatch_logs        = true
    collect_custom_metrics = true
    ec2                    = true
    ecr                    = true
    ecs                    = true
    elb                    = true
    inspector              = true
    lambda                 = true
    rds                    = true
    route53                = true
    s3                     = true
    sns                    = true
    sqs                    = true
  }
}

resource "aws_iam_role" "datadog_aws_integration" {
  name                 = "panelapp-datadog-integration"
  description          = "Role for Datadog AWS Integration"
  assume_role_policy   = data.aws_iam_policy_document.datadog_aws_integration_assume_role.json
  permissions_boundary = local.permissions_boundary
}

resource "aws_iam_policy" "datadog_aws_integration" {
  name   = "panelapp-datadog-integration"
  policy = data.aws_iam_policy_document.datadog.json
}

resource "aws_iam_role_policy_attachment" "datadog_aws_integration" {
  role       = aws_iam_role.datadog_aws_integration.name
  policy_arn = aws_iam_policy.datadog_aws_integration.arn
}

data "aws_iam_policy_document" "datadog_aws_integration_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::464622532012:root"]
    }

    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"

      values = [
        local.datadog_aws_integration_external_id,
      ]
    }
  }
}

data "aws_iam_policy_document" "datadog" {
  statement {
    sid = "1"

    actions = [
      "apigateway:GET",
      "autoscaling:Describe*",
      "budgets:ViewBudget",
      "cloudfront:GetDistributionConfig",
      "cloudfront:ListDistributions",
      "cloudtrail:DescribeTrails",
      "cloudtrail:GetTrailStatus",
      "cloudwatch:Describe*",
      "cloudwatch:Get*",
      "cloudwatch:List*",
      "cloudwatch:ListMetrics",
      "codedeploy:List*",
      "codedeploy:BatchGet*",
      "directconnect:Describe*",
      "dynamodb:List*",
      "dynamodb:Describe*",
      "ec2:Describe*",
      "ecs:Describe*",
      "ecs:List*",
      "elasticache:Describe*",
      "elasticache:List*",
      "elasticfilesystem:DescribeFileSystems",
      "elasticfilesystem:DescribeTags",
      "elasticloadbalancing:Describe*",
      "elasticmapreduce:List*",
      "elasticmapreduce:Describe*",
      "es:ListTags",
      "es:ListDomainNames",
      "es:DescribeElasticsearchDomains",
      "health:DescribeEvents",
      "health:DescribeEventDetails",
      "health:DescribeAffectedEntities",
      "kinesis:List*",
      "kinesis:Describe*",
      "lambda:AddPermission",
      "lambda:GetPolicy",
      "lambda:List*",
      "lambda:RemovePermission",
      "logs:Get*",
      "logs:Describe*",
      "logs:FilterLogEvents",
      "logs:TestMetricFilter",
      "logs:PutSubscriptionFilter",
      "logs:DeleteSubscriptionFilter",
      "logs:DescribeSubscriptionFilters",
      "rds:Describe*",
      "rds:List*",
      "redshift:DescribeClusters",
      "redshift:DescribeLoggingStatus",
      "route53:List*",
      "s3:GetBucketLogging",
      "s3:GetBucketLocation",
      "s3:GetBucketNotification",
      "s3:GetBucketTagging",
      "s3:ListAllMyBuckets",
      "s3:PutBucketNotification",
      "ses:Get*",
      "sns:List*",
      "sns:Publish",
      "sqs:ListQueues",
      "support:*",
      "tag:GetResources",
      "tag:GetTagKeys",
      "tag:GetTagValues",
      "xray:BatchGetTraces",
      "xray:GetTraceSummaries"
    ]

    resources = [
      "*",
    ]
  }
}
