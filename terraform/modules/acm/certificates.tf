// Certificates in ACM are regional resources. For Cloudfront, certificates have to be created in us-east-1.

locals {
  cert_domain_name     = "${var.dns_record}.${replace(data.aws_route53_zone.acm_domain[0].name, "/[.]$/", "")}"
  regional_cert_dvos   = var.create_regional_cert ? aws_acm_certificate.regional_cert[0].domain_validation_options : []
  cloudfront_cert_dvos = var.create_cloudfront_cert ? aws_acm_certificate.cloudfront_cert[0].domain_validation_options : []
}

data "aws_route53_zone" "acm_domain" {
  count    = var.create_regional_cert || var.create_cloudfront_cert ? 1 : 0
  provider = aws.dns
  zone_id  = var.public_zone_id
}

resource "aws_acm_certificate" "regional_cert" {
  count                     = var.create_regional_cert ? 1 : 0
  domain_name               = local.cert_domain_name
  subject_alternative_names = []
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = local.cert_domain_name
  }
}

resource "aws_acm_certificate" "cloudfront_cert" {
  provider                  = aws.us_east_1
  count                     = var.create_cloudfront_cert ? 1 : 0
  domain_name               = local.cert_domain_name
  subject_alternative_names = []
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = local.cert_domain_name
  }
}

resource "aws_route53_record" "regional_cert_validation" {
  provider = aws.dns

  for_each = {
    for dvo in local.regional_cert_dvos : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name            = each.value.name
  type            = each.value.type
  zone_id         = data.aws_route53_zone.acm_domain[0].zone_id
  records         = [each.value.record]
  ttl             = 60
  allow_overwrite = true
}

resource "aws_route53_record" "cloudfront_cert_validation" {
  provider = aws.dns

  for_each = {
    for dvo in local.cloudfront_cert_dvos : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name            = each.value.name
  type            = each.value.type
  zone_id         = data.aws_route53_zone.acm_domain[0].zone_id
  records         = [each.value.record]
  ttl             = 60
  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "cloudfront_cert" {
  count                   = var.create_cloudfront_cert ? 1 : 0
  provider                = aws.us_east_1
  certificate_arn         = aws_acm_certificate.cloudfront_cert[0].arn
  validation_record_fqdns = [for record in aws_route53_record.cloudfront_cert_validation : record.fqdn]
  timeouts {
    create = "10m"
  }
}

resource "aws_acm_certificate_validation" "regional_cert" {
  count                   = var.create_regional_cert ? 1 : 0
  certificate_arn         = aws_acm_certificate.regional_cert[0].arn
  validation_record_fqdns = [for record in aws_route53_record.regional_cert_validation : record.fqdn]
  timeouts {
    create = "10m"
  }
}
