terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      configuration_aliases = [
        aws, aws.dns, aws.us_east_1
      ]
    }
  }
}
