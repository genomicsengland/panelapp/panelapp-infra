resource "aws_kms_key" "automation" {
  description = "PanelApp automation key ${var.name.ws_env_product}"
  policy      = data.aws_iam_policy_document.automation_key.json
}

resource "aws_kms_alias" "automation" {
  name          = "alias/${var.name.ws_env_product}-automation"
  target_key_id = aws_kms_key.automation.key_id
}

data "aws_iam_policy_document" "automation_key" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }

    resources = ["*"]
    actions   = ["kms:*"]
  }
}
