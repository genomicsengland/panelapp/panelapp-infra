variable "bucket" {
  type = object({
    name = string
    arn  = string
  })
}

variable "service_role" {
  type = string
}

variable "cloudfront_oai" {
  type = string
}
