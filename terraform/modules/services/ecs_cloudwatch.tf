locals {
  log_groups = [
    "web",
    "web-datadog_agent",
    "web-firelens",
    "worker",
    "worker-datadog_agent",
    "worker-firelens",
    "worker_beat",
    "worker_beat-datadog_agent",
    "worker_beat-firelens",
    "migrate",
    "collect_static",
    "data_cleanup",
    "ensembl_id_update"
  ]
}

resource "aws_cloudwatch_log_group" "panelapp" {
  for_each = toset(local.log_groups)

  name              = "/${var.name.ws_product}/${each.key}"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "${var.name.ws_product}-${each.key}"
    Application = "${var.name.ws_product}-${each.key}"
  }
}


# FIXME: no longer used; remove 1 month after deployment
resource "aws_cloudwatch_log_group" "panelapp_web" {
  count             = local.is_default_ws_count
  name              = "panelapp-web"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-web"
    Application = "panelapp-web"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_web_datadog_agent" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_web_datadog_agent"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-web"
    Application = "panelapp-web"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_web_firelens" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_web_firelens"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-web"
    Application = "panelapp-web"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker" {
  count             = local.is_default_ws_count
  name              = "panelapp-worker"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker"
    Application = "panelapp-worker"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker_datadog_agent" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_worker_datadog_agent"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker"
    Application = "panelapp-worker"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker_firelens" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_worker_firelens"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker"
    Application = "panelapp-worker"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker_beat" {
  count             = local.is_default_ws_count
  name              = "panelapp-worker-beat"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker_beat"
    Application = "panelapp-worker-beat"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker_beat_datadog_agent" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_worker_beat_datadog_agent"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker-beat"
    Application = "panelapp-worker_beat"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_worker_beat_firelens" {
  count             = local.is_default_ws_count
  name              = "/aws/ecs/panelapp_worker_beat_firelens"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp-worker-beat"
    Application = "panelapp-worker-beat"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_migrate" {
  count             = local.is_default_ws_count
  name              = "panelapp-migrate"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp_migrate"
    Application = "panelapp-other"
  }
}

resource "aws_cloudwatch_log_group" "panelapp-collectstatic" {
  count             = local.is_default_ws_count
  name              = "panelapp-collectstatic"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp_collectstatic"
    Application = "panelapp-other"
  }
}

resource "aws_cloudwatch_log_group" "panelapp_datacleanup" {
  count             = local.is_default_ws_count
  name              = "panelapp-datacleanup"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp_datacleanup"
    Application = "panelapp-other"
  }
}

resource "aws_cloudwatch_log_group" "panelapp-oneoff" {
  count             = local.is_default_ws_count
  name              = "panelapp-oneoff"
  retention_in_days = local.log_retention
  kms_key_id        = var.kms_key_arn

  tags = {
    Name        = "panelapp_oneoff"
    Application = "panelapp-other"
  }
}
