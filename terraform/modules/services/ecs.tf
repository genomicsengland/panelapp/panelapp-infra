resource "aws_ecs_cluster" "panelapp_cluster" {
  # FIXME: Phase 2 (downtime)
  #name = var.name.ws_product
  name = local.is_default_ws ? "panelapp-cluster-${var.env_name}" : var.name.ws_product
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}
