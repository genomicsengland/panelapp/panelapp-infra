variable "env_name" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type = object({
    product        = string
    ws_product     = string
    ws_env_product = string
    #    dns            = string
    bucket = string
  })
  description = "Workspace aware name parts"
}

variable "vpc_id" {
  type = string
}

variable "cdn_alias" {
  type        = string
  description = "CDN alias"
}

variable "static_cdn_alias" {
  type        = string
  description = "CDN alias"
}

variable "media_cdn_alias" {
  type        = string
  description = "CDN alias"
}

variable "gmspanels_url" {
  type = string
}

variable "certificate_arn" {
  type        = string
  description = "Regional Certificate ARN"
}

variable "task" {
  type = object({
    default = object({
      cpu    = number
      memory = number
    })
    web = object({
      cpu    = number
      memory = number
    })
    worker = object({
      cpu    = number
      memory = number
    })
    worker_beat = object({
      cpu    = number
      memory = number
    })
  })
  default = {
    default = {
      cpu    = 4096
      memory = 8192
    }
    web = {
      cpu    = 4096
      memory = 8192
    }
    worker = {
      cpu    = 2048
      memory = 4096
    }
    worker_beat = {
      cpu    = 512
      memory = 1024
    }
  }
}

variable "database" {
  type = object({
    writer_endpoint     = string
    port                = number
    name                = string
    user                = string
    master_password_arn = string
  })
}

variable "session" {
  type = object({
    cookie_age = number
  })
}

variable "email" {
  type = object({
    sender_address  = string
    contact_address = string
    smtp_server     = string
    smtp_port       = number
  })
}

variable "django" {
  type = object({
    log_level       = string
    settings_module = string
    admin_email     = string
  })
}

variable "panelapp" {
  type = object({
    tasks = object({
      web    = number
      worker = number
    })
    workers            = number
    connection_timeout = number
    access_log         = string
  })
}

variable "buckets" {
  type = object({
    statics   = object({ name = string, arn = string })
    media     = object({ name = string, arn = string })
    upload    = object({ name = string, arn = string })
    artifacts = object({ name = string, arn = string })
  })
}

variable "datadog_tags_map" {
  type = map(string)
}

variable "kms_key_arn" {
  type = string
}

variable "docker_image" {
  type = string
}

variable "scheduled_tasks" {
  type = object({
    tasks  = list(string)
    config = map(string)
  })
}

variable "side_cars" {
  description = "Repository:version of side cars from core_bakery"
  type = object({
    datadog_agent = string
    fluentbit     = string
  })
}
