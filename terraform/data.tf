data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_iam_roles" "admin_role" {
  name_regex = "AWSReservedSSO_AdministratorAccess_*"
}
