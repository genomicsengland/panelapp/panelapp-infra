module "aws_tags" {
  # checkov:skip=CKV_TF_1: internal module
  # checkov:skip=CKV_TF_2: use latest as per module documentation
  source      = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/gel-standard-tagging?ref=latest"
  tribe       = "evolving genomic healthcare"
  gel_service = "help clinicians order genomic tests"
  squad       = "knowledge management squad"

  service     = "PanelApp"
  application = "panelapp"
  environment = var.env_name
  additional_tags = {
    terraform_workspace = terraform.workspace
  }
}
