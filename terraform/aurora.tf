module "aurora" {
  source = "./modules/aurora"

  name     = local.name
  env_name = var.env_name
  vpc_id   = var.vpc_id

  engine_version               = var.aurora.engine_version
  skip_final_snapshot          = true
  database                     = "panelapp"
  username                     = "panelapp"
  master_password              = local.aws_ssm_parameter_aurora_master_password.value
  auto_minor_version_upgrade   = var.aurora.auto_minor_version_upgrade
  preferred_maintenance_window = "Mon:00:00-Mon:01:00"
  rds_snapshot                 = var.snapshot_identifier
  trusted_accounts             = [for k, v in local.rds_share_snapshot_trusted_accounts : v]
  cluster_size                 = var.aurora.cluster_size
  instance_class               = var.aurora.instance_class
  extra_tags                   = local.backup_tags
}

resource "aws_security_group_rule" "inbound_fargate" {
  description              = "Fargate to Aurora"
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = module.aurora.security_group
  source_security_group_id = module.services.security_group
}

resource "aws_security_group_rule" "inbound_mgmt" {
  description              = "EC2 mgt box to Aurora"
  count                    = var.create_mgmt_box ? 1 : 0
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = module.aurora.security_group
  source_security_group_id = module.mgt[0].security_group
}

resource "aws_ssm_parameter" "aurora_master_password" {
  count       = local.is_default_ws_count
  name        = "/panelapp/database/master_password"
  description = "DB password"
  type        = "SecureString"
  key_id      = local.kms_key_arn
  value       = "change_me"

  lifecycle {
    ignore_changes = [
      value,
    ]
  }
}

data "aws_ssm_parameter" "aurora_master_password" {
  count = 1 - local.is_default_ws_count
  name  = "/panelapp/database/master_password"
}

locals {
  aws_ssm_parameter_aurora_master_password = local.is_default_ws ? aws_ssm_parameter.aurora_master_password[0] : data.aws_ssm_parameter.aurora_master_password[0]
}
