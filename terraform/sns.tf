resource "aws_sns_topic" "noreply" {
  count = local.is_default_ws_count
  name  = "panelapp-${var.env_name}-bounce-topic"
}

resource "aws_sns_topic_policy" "noreply" {
  count  = local.is_default_ws_count
  arn    = aws_sns_topic.noreply[0].arn
  policy = data.aws_iam_policy_document.noreply[0].json
}

data "aws_iam_policy_document" "noreply" {
  count     = local.is_default_ws_count
  policy_id = "panelapp-${var.env_name}-policy-noreply"

  statement {
    sid    = "panelapp-bounce-sns-access"
    effect = "Allow"
    actions = [
      "sns:Publish"
    ]
    resources = [
      aws_sns_topic.noreply[0].arn,
    ]
    principals {
      type = "Service"
      identifiers = [
        "ses.amazonaws.com"
      ]
    }
  }
}
