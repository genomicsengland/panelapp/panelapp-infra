module "acm_gel" {
  source = "./modules/acm"

  create_regional_cert   = true
  create_cloudfront_cert = false

  public_zone_id = data.aws_route53_zone.internal_domain.zone_id
  dns_record     = local.dns.app.internal

  env_name            = var.env_name
  account_id          = var.account_id
  region              = var.region
  core_shared_account = local.core_shared_account

  providers = {
    aws           = aws
    aws.dns       = aws.dns
    aws.us_east_1 = aws.us_east_1
  }
}

module "media_acm_genomicsengland" {
  source = "./modules/acm"

  create_regional_cert   = false
  create_cloudfront_cert = true

  public_zone_id = data.aws_route53_zone.external_domain.zone_id
  dns_record     = local.dns.media.external

  env_name            = var.env_name
  account_id          = var.account_id
  region              = var.region
  core_shared_account = local.core_shared_account

  providers = {
    aws           = aws
    aws.dns       = aws.dns
    aws.us_east_1 = aws.us_east_1
  }
}

module "static_acm_genomicsengland" {
  source = "./modules/acm"

  create_regional_cert   = false
  create_cloudfront_cert = true

  public_zone_id = data.aws_route53_zone.external_domain.zone_id
  dns_record     = local.dns.static.external

  env_name            = var.env_name
  account_id          = var.account_id
  region              = var.region
  core_shared_account = local.core_shared_account

  providers = {
    aws           = aws
    aws.dns       = aws.dns
    aws.us_east_1 = aws.us_east_1
  }
}
