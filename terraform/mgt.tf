module "mgt" {
  source = "./modules/mgt"
  count  = var.create_mgmt_box ? 1 : 0

  name = local.name
  tags = module.aws_tags.tags

  instance_type = "t2.large"
  vpc_id        = var.vpc_id

  ebs_key_arn            = local.kms_key_arn
  image_name             = "${local.ecr_registry}/panelapp:${var.image_tag}"
  database_port          = module.aurora.port
  database_name          = module.aurora.database_name
  database_user          = module.aurora.database_user
  database_host          = module.aurora.writer_endpoint
  panelapp_statics       = local.cdn_buckets.statics.name
  panelapp_media         = local.cdn_buckets.media.name
  cdn_domain_name        = "${local.dns.app.external}.${local.dns.zone.external}"
  artifacts_bucket       = module.automation.buckets.artifacts.name
  django_settings_module = local.django_settings_module
}

resource "aws_security_group_rule" "mgmt_egress_database" {
  count                    = var.create_mgmt_box ? 1 : 0
  type                     = "egress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = module.mgt[0].security_group
  source_security_group_id = module.aurora.security_group
  description              = "Allow Postgres to Database"
}
