data "aws_route53_zone" "internal_domain" {
  provider     = aws.dns
  name         = local.dns.zone.internal
  private_zone = false
}

data "aws_route53_zone" "external_domain" {
  provider     = aws.dns
  name         = local.dns.zone.external
  private_zone = false
}

resource "aws_route53_record" "app_internal" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.internal_domain.zone_id
  name     = local.dns.app.internal
  type     = "A"

  alias {
    name                   = module.services.elb_dns.name
    zone_id                = module.services.elb_dns.zone_id
    evaluate_target_health = false
  }
}
