resource "aws_ssm_parameter" "panelapp_application_version" {
  name           = "/${local.name.ws_product}/version/application"
  description    = "Currently deployed PanelApp application version"
  type           = "String"
  insecure_value = var.image_tag
}

resource "aws_ssm_parameter" "panelapp_infrastructure_version" {
  name           = "/${local.name.ws_product}/version/infrastructure"
  description    = "Currently deployed PanelApp infrastructure version"
  type           = "String"
  insecure_value = var.infra_tag
}
