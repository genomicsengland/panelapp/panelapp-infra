locals {
  # FIXME: switch over to new cdn later
  cdn_buckets = local.is_default_ws ? module.cdn[0].bucket : module.cdn_ws.bucket
}

module "cdn" {
  count  = local.is_default_ws_count
  source = "./modules/cdn_old"

  env_name = var.env_name

  dns = {
    media_fqdn  = "${local.dns.media.external}.${local.dns.zone.external}"
    static_fqdn = "${local.dns.static.external}.${local.dns.zone.external}"
  }
  certificate_arns = {
    media   = module.media_acm_genomicsengland.cloudfront_cert
    statics = module.static_acm_genomicsengland.cloudfront_cert
  }

  backup_tags = local.backup_tags

  providers = {
    aws           = aws
    aws.us_east_1 = aws.us_east_1
  }
}

module "media_policy" {
  count          = local.is_default_ws_count
  source         = "./modules/cdn_policy"
  bucket         = module.cdn[0].bucket.media
  cloudfront_oai = module.cdn[0].cloudfront_oai
  service_role   = module.services.ecs_task_iam_role
}

module "statics_policy" {
  count          = local.is_default_ws_count
  source         = "./modules/cdn_policy"
  bucket         = module.cdn[0].bucket.statics
  cloudfront_oai = module.cdn[0].cloudfront_oai
  service_role   = module.services.ecs_task_iam_role
}

module "cdn_ws" {
  source = "./modules/cdn"

  name = local.name
  dns  = local.dns
  certificate_arns = {
    media   = module.media_acm_genomicsengland.cloudfront_cert
    statics = module.static_acm_genomicsengland.cloudfront_cert
  }

  backup_tags = local.backup_tags

  providers = {
    aws           = aws
    aws.dns       = aws.dns
    aws.us_east_1 = aws.us_east_1
  }
}

module "media_policy_ws" {
  source           = "./modules/cdn_policy_ws"
  bucket           = module.cdn_ws.bucket.media
  service_role     = module.services.ecs_task_iam_role
  distribution_arn = module.cdn_ws.distribution.media
}

module "statics_policy_ws" {
  source           = "./modules/cdn_policy_ws"
  bucket           = module.cdn_ws.bucket.statics
  service_role     = module.services.ecs_task_iam_role
  distribution_arn = module.cdn_ws.distribution.static
}

module "cloudflare" {
  source   = "./modules/cdn_cloudflare"
  env_name = var.env_name
  providers = {
    aws.secrets = aws.secrets
  }
}
