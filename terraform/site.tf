module "site" {
  source = "./modules/site"
  count  = local.is_default_ws_count

  env_name   = var.env_name
  account_id = var.account_id
  region     = var.region
}
