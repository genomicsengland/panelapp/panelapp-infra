module "automation" {
  source              = "./modules/automation"
  env_name            = var.env_name
  name                = local.name
  default_tags        = module.aws_tags.tags
  backup_tags         = local.backup_tags
  boundary_policy_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/GELBoundary"

  vpc_id                = var.vpc_id
  ecs_cluster           = module.services.cluster.name
  ecs_services          = module.services.services
  standalone_tasks      = module.services.standalone_tasks
  ecs_security_group_id = module.services.security_group
  ecs_task_iam_role_arn = module.services.ecs_task_iam_role
  log_groups            = module.services.log_groups
  ssm_parameters = {
    panelapp_banner = module.services.ssm_parameters.panelapp_banner
    aurora_snapshot = module.aurora.rds_snapshot_ssm_parameter
  }
  aurora = {
    cluster = module.aurora.aurora_cluster
    parameter_groups = {
      read-only  = module.aurora.aurora_cluster_parameter_groups.read-only
      read-write = module.aurora.aurora_cluster_parameter_groups.read-write
    }
    accounts_to_share_with = [for k, v in local.rds_share_snapshot_trusted_accounts : v if k != "prod"]
    accounts_to_copy_from  = var.env_name == "prod" ? [] : [for k, v in local.rds_share_snapshot_trusted_accounts : v]
    encryption_key_arn     = module.aurora.rds_shared_key
    decryption_key_arns    = [for k, v in local.rds_key_arns : v]
  }
  email = local.email
}
