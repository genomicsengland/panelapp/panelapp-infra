# Terraform refactoring

# PANELAPP-1930
moved {
  from = module.aurora.aws_kms_key.rds_shared
  to   = module.aurora.aws_kms_key.rds_shared[0]
}

moved {
  from = module.aurora.aws_kms_alias.rds_shared
  to   = module.aurora.aws_kms_alias.rds_shared[0]
}
