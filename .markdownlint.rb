all

rule 'MD007', :indent => 4  # indentation of unordered list items, must be >=3
rule 'MD013', :line_length => 120  # maximum line length
rule 'MD024', :allow_different_nesting => true
rule 'MD029', :style => :ordered  # whether ordered lists are 1., 1., ... (:one) or 1., 2., ... (:ordered)
