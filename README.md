# Cloud-PanelApp AWS infra-as-code

## Project structure and multi-environment support

This code has been designed to be multi-environment (e.g. "dev", test", "uat", "prod").

Each environment is supposed to be installed in a **separate AWS account**.

The Terraform code for the environments is split into two separate Terraform projects (components):

### Configuration

Each AWS account (one per environment) needs a separate configuration.

The configuration of each Terraform component has two parts:

1. `terraform/vars/<env>.tfvars`, containing all parameters specific to the account/environment.
2. `terraform/backends/<env>.backend`, to configure Terraform state backend (S3 and DynamoDB).

### Local testing

```shell
docker-compose run terraform
```

Inside the container:

```shell
apk add make

make init

make format

make validate
```

### Code Quality

This project uses [pre-commit](https://pre-commit.com/) framework to run code linter, style checker etc.

Initialize the Python environment

```shell
make venv
````

Then the checks can be run by

```shell
pre-commit run --all-files
```

The pre-commit hooks are defined in [pre-commit config](./.pre-commit-config.yaml). To run all hooks before commit, use

```shell
pre-commit install
```

The hooks used

| hook                                                               | language   |
|:-------------------------------------------------------------------|:-----------|
| [pre-commit-hooks](https://github.com/pre-commit/pre-commit-hooks) | generic    |
| [terraform](https://github.com/AleksaC/terraform-py)               | Terraform  |
| [black](https://github.com/psf/black)                              | Python     |
| [isort](https://github.com/timothycrosley/isort)                   | Python     |
| [flake8](https://github.com/pycqa/flake8)                          | Python     |
| [bandit](https://github.com/PyCQA/bandit)                          | Python     |
| [mypy](https://github.com/python/mypy)                             | Python     |
| [shellcheck](https://www.shellcheck.net/)                          | Shell/Bash |
| [checkmate](https://github.com/mrtazz/checkmake.git)               | Makefile   |
| [markdownlint](https://github.com/markdownlint/markdownlint)       | Markdown   |
| [checkov](https://github.com/bridgecrewio/checkov.git)             | Terraform  |
| [gitleaks](https://github.com/gitleaks/gitleaks)                   | generic    |
